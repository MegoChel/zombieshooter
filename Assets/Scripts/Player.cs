using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class Player : MonoBehaviour
{
    CursorManager cursor;
    NavMeshAgent navMeshAgent;
    Shot shot;
    public float moveSpeed = 1;
    public Transform gunBarrel;
    float HP = 100;
    TextMeshProUGUI textMeshHP;
    bool dead;
    Animator animator;
    float handgunHeight = 0.05454171f;
    public float Damage = 45;
    public float DamageOffset = 15;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.updateRotation = false;
        cursor = FindObjectOfType<CursorManager>();
        shot = FindObjectOfType<Shot>();
        textMeshHP = GameObject.Find("HP Bar").GetComponent<TextMeshProUGUI>();
        animator = GetComponentInChildren<Animator>();
        handgunHeight = gunBarrel.position.y;
    }

    public void DealDamage(float damage)
    {
        if (!dead)
        {
            HP -= damage;
            textMeshHP.text = "HP: " + Mathf.Round(HP);
            if (HP < 0)
                Kill();
        }
    }

    void Kill()
    {
        if (!dead)
        {
            dead = true;
            animator.SetTrigger("died");
            textMeshHP.text = "HP: 0";
        }
    }

    public void Heal(float hp, GameObject medicBag)
    {
        if (HP > 99.5)
            return;
        HP += hp;
        HP = Mathf.Clamp(HP, 0, 100);
        textMeshHP.text = "HP: " + Mathf.Round(HP);
        Destroy(medicBag);
    }

    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            Vector3 dir = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
                dir.z = 1;
            if (Input.GetKey(KeyCode.A))
                dir.x = -1;
            if (Input.GetKey(KeyCode.S))
                dir.z = -1;
            if (Input.GetKey(KeyCode.D))
                dir.x = 1;
            navMeshAgent.velocity = dir.normalized * moveSpeed;

            if (Input.GetMouseButtonDown(0))
            {
                var from = gunBarrel.position;
                var target = cursor.transform.position;
                var to = new Vector3(target.x, from.y, target.z);

                var direction = (to - from).normalized;

                RaycastHit hit;
                if (Physics.Raycast(from, direction, out hit, 100))
                {
                    if (hit.transform)
                    {
                        var zombie = hit.transform.GetComponent<Zombie>();
                        if (zombie)
                            zombie.DealDamage(Random.Range(Damage - DamageOffset, Damage + DamageOffset));
                    }

                    to = new Vector3(hit.point.x, from.y, hit.point.z);
                }
                else
                {
                    to = from + direction * 100;
                }

                shot.Show(from, to);

            }
            Vector3 forward = cursor.transform.position - transform.position;
            transform.rotation = Quaternion.LookRotation(new Vector3(forward.x, 0, forward.z));
        }


    }
}
