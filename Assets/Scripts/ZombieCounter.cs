using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZombieCounter : MonoBehaviour
{
    TextMeshProUGUI text;
    WaveSystem waveSystem;
    int Count = 0;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        waveSystem = FindObjectOfType<WaveSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddKill()
    {
        Count++;
        text.text = "Zombie Killed: " + Count;
        waveSystem.UpdateWave(Count);
    }
}
