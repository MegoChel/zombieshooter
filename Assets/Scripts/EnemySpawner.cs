using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    public float Period;
    public GameObject Enemy;
    public float range = 5.0f;
    private float TimeUntilNextSpawn;
    WaveSystem waveSystem;

    // Start is called before the first frame update
    void Start()
    {
        TimeUntilNextSpawn = Random.Range(0, Period);
        waveSystem = FindObjectOfType<WaveSystem>();
    }
    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }
    // Update is called once per frame
    void Update()
    {
        TimeUntilNextSpawn -= Time.deltaTime;
        if (TimeUntilNextSpawn < 0)
        {
            for (int i = 0; i < waveSystem.GetWave(); i++)
            {
                Vector3 sp;
                RandomPoint(gameObject.transform.position, range, out sp);
                TimeUntilNextSpawn = Period;
                Instantiate(Enemy, sp, transform.rotation);
            }

        }
    }
}
