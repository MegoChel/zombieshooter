using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    Player player;
    public float HPRestore = 30;
    public float speed = 100;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.GetComponent<Player>())
        {
            player.Heal(HPRestore, gameObject);
        }
    }



    // Update is called once per frame
    void Update()
    {
        transform.Rotate(transform.up * speed * Time.deltaTime);
    }
}
