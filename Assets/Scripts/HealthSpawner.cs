using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HealthSpawner : MonoBehaviour
{
    public float range = 10.0f;
    public GameObject megicBag;
    public float spawnTime = 20;
    float time;
    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        time = spawnTime;

    }
    void Spawn()
    {
        Vector3 sp;
        RandomPoint(gameObject.transform.position, range, out sp);
        Instantiate(megicBag, sp, gameObject.transform.rotation);

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(time);
        if (time < 0)
        {
            Spawn();
            time = spawnTime;
        }
        time -= Time.deltaTime;
    }
}
