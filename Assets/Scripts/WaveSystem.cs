using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WaveSystem : MonoBehaviour
{
    TextMeshProUGUI textMesh;
    ZombieCounter zombieCounter;
    int WaveNumber;
    // Start is called before the first frame update
    void Start()
    {
        zombieCounter = FindObjectOfType<ZombieCounter>();
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateWave(int zombieKilled)
    {
        if (Mathf.IsPowerOfTwo(zombieKilled)) //zombieKilled >= Mathf.Pow(WaveNumber, 2)
        {
            WaveNumber++;
            textMesh.text = "Wave: " + WaveNumber;
        }
    }

    public int GetWave()
    {
        return WaveNumber;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
