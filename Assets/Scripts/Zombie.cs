using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    Player player;
    NavMeshAgent navMeshAgent;
    CapsuleCollider capsuleCollider;
    bool dead;
    Animator animator;
    MovementAnimator movement;
    ParticleSystem particleSystem;
    ZombieCounter zombieCounter;
    float HP = 100;
    public float Damage = 7;
    public float DamageModifier = 0.5f;
    public float AttackFrequency = 1;
    public float AttackDistantion = 1;
    float AttackTimer;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        zombieCounter = FindObjectOfType<ZombieCounter>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        animator = GetComponentInChildren<Animator>();
        movement = GetComponentInChildren<MovementAnimator>();
        particleSystem = GetComponentInChildren<ParticleSystem>();

        HP = 100;
        DamageModifier = Mathf.Clamp(DamageModifier, 0, 1);
        AttackTimer = AttackFrequency;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dead)
            return;

        navMeshAgent.SetDestination(player.transform.position);
        AttackPlayer();
    }

    void Kill()
    {
        if (!dead)
        {
            dead = true;
            Destroy(capsuleCollider);
            Destroy(movement);
            Destroy(navMeshAgent);
            animator.SetTrigger("died");
            particleSystem.Play();
            zombieCounter.AddKill();
        }
    }
    public void DealDamage(float damage)
    {

        HP -= damage;
        if (HP < 0)
            Kill();
    }
    void AttackPlayer()
    {
        if (AttackTimer < 0)
        {
            if ((transform.position - player.transform.position).magnitude < AttackDistantion)
            {
                animator.SetTrigger("attack");
                player.DealDamage(Random.Range((1 - DamageModifier) * Damage, (1 + DamageModifier) * Damage));
                AttackTimer = AttackFrequency;
            }
        }
        AttackTimer -= Time.deltaTime;
    }
}
