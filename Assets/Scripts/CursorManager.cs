using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{

    SpriteRenderer spriteRenderer;
    int layerMask;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        layerMask = LayerMask.GetMask("Ground");
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000, layerMask))
        {
            transform.position = new Vector3(hit.point.x, hit.point.y + 0.01f, hit.point.z);
            transform.rotation = Quaternion.Euler(hit.normal);
            spriteRenderer.enabled = true;
        } 
        else
        {
            spriteRenderer.enabled = false;
        }

    }
}
